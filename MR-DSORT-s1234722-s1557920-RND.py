import MapReduce

"""
    Dennis van der Zwet s1234722
    Jan Thiemen Postema s1557920
"""


def mapper(key, value):
    # The number of reduce task should be a small multiple of the number of chunk servers; in our case 6 times 12 chunk
    # servers.
    size = 6 * 12
    splitvalue = 10.0**6/size

    for integer in value:
        mr.emit_intermediate(int(integer/splitvalue), integer)


def reducer(key, list_of_values):
    mr.emit((key, sorted(list_of_values)))


# ____________________________________________________________________________
# This code remains unmodified in all programs, except for the input file name.

if __name__ == '__main__':
    data = open("integers.json")
    mr = MapReduce.MapReduce()
    mr.execute(data, mapper, reducer)
