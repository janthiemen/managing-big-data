from pyspark import SparkContext
from pyspark.sql import SQLContext

"""
    Dennis van der Zwet s1234722
    Jan Thiemen Postema s1557920
"""

sc = SparkContext("local", "Twitter")
sc.setLogLevel("ERROR")

sqlc = SQLContext(sc)
df = sqlc.read.json("/data/doina/twitterNL/201612/20161231-23.out.gz")

tweets = df.select("id", "entities.hashtags", "text")
top_tweets = tweets.rdd.flatMap(lambda row : [(r.text,1)  for r in row.hashtags])\
	.reduceByKey(lambda a,b: a+b)\
	.sortBy(lambda record: record[1], ascending=False)\
	.take(20)

for (tweet, count) in top_tweets:
	print (tweet,count)
