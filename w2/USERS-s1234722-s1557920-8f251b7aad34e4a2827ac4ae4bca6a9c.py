from pyspark import SparkContext
from pyspark.sql import SQLContext

"""
    Dennis van der Zwet s1234722
    Jan Thiemen Postema s1557920
"""

sc = SparkContext("local", "Twitter")
sc.setLogLevel("ERROR")

sqlc = SQLContext(sc)
df = sqlc.read.json("/data/doina/twitterNL/201612/20161231-23.out.gz")

users = df.select("id", "user.screen_name", "text")
top_users = users.rdd.map(lambda row : (row.screen_name, 1) )\
	.reduceByKey(lambda a,b: a+b)\
	.sortBy(lambda record: record[1], ascending=False)\
	.take(40)

for (user, count) in top_users:
	print (user,count)