import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import pickle
N=1000
with open ('musicdata_x', 'rb') as fp:
    x = pickle.load(fp)

with open ('musicdata_axis', 'rb') as fp:
    axis = pickle.load(fp)


plt.bar(axis, x, width= 1.0)
plt.xlabel('Bucket ('+str(N)+' sales ranks each)')
plt.ylabel('Count of products/bucket')
plt.title('Sales ranks for Amazon Music')


plt.savefig('MUSIC-s1234722-s1557920-8f251b7aad34e4a2827ac4ae4bca6a9c.pdf')
plt.show()
