from pyspark import SparkContext
from pyspark.sql import SQLContext

"""
    Dennis van der Zwet s1234722
    Jan Thiemen Postema s1557920
"""

sc = SparkContext("local", "Twitter")
sc.setLogLevel("ERROR")

sqlc = SQLContext(sc)
df = sqlc.read.json("/data/doina/UCSD-Amazon-Data/meta_Books_sample.json.gz")

products = df.select("asin", "related.also_bought")
#collect the ids per sales rank
also_boughts = products.rdd.filter(lambda row : row.also_bought != None).flatMap(lambda row : row.also_bought)\
	.map(lambda asin: (asin,1))\
	.reduceByKey(lambda a,b: a+b)\
	.max(lambda x:x[1])

#collect full record if exists
record = df.filter(df.asin == also_boughts[0] ).first()

print "Count: " + str(also_boughts[1])
print "Asin: " + also_boughts[0]
if record != None:
	print "Record: " + str(record)
