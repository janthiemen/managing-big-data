from __future__ import print_function
from pyspark import SparkContext

"""
    Dennis van der Zwet s1234722
    Jan Thiemen Postema s1557920
"""

sc = SparkContext("local", "Inverted index")
sc.setLogLevel("ERROR")

path = "/data/doina/Gutenberg-EBooks"
inverted_index  = sc.wholeTextFiles(path)\
	.map(lambda (file, contents) : (file, contents.split()))\
	.flatMap(lambda (file, contents) : [(word, [file]) for word in contents])\
	.reduceByKey(lambda a, b : a+b)\
	.filter(lambda (k,v) : len(v)==14)\
	.collect()


for (w, fs) in inverted_index:
	print(w.encode('utf-8'), end=' ')

print(" ")
