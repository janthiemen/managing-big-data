import MapReduce

"""
    Dennis van der Zwet s1234722
    Jan Thiemen Postema s1557920
"""


def mapper(key, value):
    mr.emit_intermediate(0, max(value))


def reducer(key, list_of_values):
    mr.emit(max(list_of_values))


# ____________________________________________________________________________
# This code remains unmodified in all programs, except for the input file name.

if __name__ == '__main__':
    data = open("integers.json")
    mr = MapReduce.MapReduce()
    mr.execute(data, mapper, reducer)
