import MapReduce
import re
"""
    Dennis van der Zwet s1234722
    Jan Thiemen Postema s1557920
"""


def mapper(key, value):
    hashtags = re.findall("[#]\w+", value["text"])
    hashtags = [hashtag.lower() for hashtag in hashtags]
    word_counter = Counter(hashtags)

    for w in word_counter:
        mr.emit_intermediate(w, word_counter[w])


def reducer(key, list_of_values):
    appearences = sum(list_of_values)
    if appearences >= 20:
        mr.emit((key, appearences))


# ____________________________________________________________________________
# This code remains unmodified in all programs, except for the input file name.

if __name__ == '__main__':
    data = open("one_hour_of_tweets.json")
    mr = MapReduce.MapReduce()
    mr.execute(data, mapper, reducer)
