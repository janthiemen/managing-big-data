from collections import Counter

import MapReduce

"""
    Dennis van der Zwet s1234722
    Jan Thiemen Postema s1557920
"""


def mapper(key, value):
    words = value.split()
    words = [word.lower() for word in words]
    word_counter = Counter(words)

    for w in word_counter:
        mr.emit_intermediate(w, word_counter[w])


def reducer(key, list_of_values):
    mr.emit((key, sum(list_of_values)))


# ____________________________________________________________________________
# This code remains unmodified in all programs, except for the input file name.

if __name__ == '__main__':
    data = open("book_pages.json")
    mr = MapReduce.MapReduce()
    mr.execute(data, mapper, reducer)
