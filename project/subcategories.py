from __future__ import unicode_literals
from __future__ import division
from pyspark import SparkContext
from pyspark import SQLContext
import urllib2
import sys
import os
import array
from collections import namedtuple
import time

file = "meta_Movies_and_TV"
path = "/data/doina/UCSD-Amazon-Data/"+file+".json.gz"
sc = SparkContext(appName="Amazon colours")
# sc = SparkContext("local", "project")
sqlc = SQLContext(sc)
df = sqlc.read.json(path)

files = ["/data/doina/UCSD-Amazon-Data/meta_Cell_Phones_and_Accessories.json.gz",
    "/data/doina/UCSD-Amazon-Data/meta_Clothing_Shoes_and_Jewelry.json.gz",
    "/data/doina/UCSD-Amazon-Data/meta_Electronics.json.gz",
    "/data/doina/UCSD-Amazon-Data/meta_Home_and_Kitchen.json.gz",
    "/data/doina/UCSD-Amazon-Data/meta_Office_Products.json.gz",
    "/data/doina/UCSD-Amazon-Data/meta_Sports_and_Outdoors.json.gz",
    "/data/doina/UCSD-Amazon-Data/meta_Tools_and_Home_Improvement.json.gz",
    "/data/doina/UCSD-Amazon-Data/meta_Toys_and_Games.json.gz"]

# dataframes = map(lambda r: sqlc.read.json(r), files)
# union = reduce(lambda df1, df2: df1.select("asin","imUrl","salesRank","price").unionAll(df2.select("asin","imUrl","salesRank","price")), dataframes)

rdds = [sqlc.read.json(file).select("asin","imUrl","salesRank","price").rdd for file in files]
union = sc.union(rdds)

# useful_information = df.select("asin","imUrl","salesRank","price").rdd.sample(False, 0.001)\
# useful_information = union.select("asin","imUrl","salesRank","price").rdd.repartition(3000)\
useful_information = union.repartition(3000)\
    .filter(lambda x: x["salesRank"] != None and x["imUrl"] != None)\
    .map(lambda x: (x["asin"], x["imUrl"], extractrgb(x["imUrl"]),next(iter([i for i in x["salesRank"].asDict() if x["salesRank"][i] != None] or []), None),next(iter([x["salesRank"][i] for i in x["salesRank"].asDict() if x["salesRank"][i] != None] or []), None),x["price"]))\
    .filter(lambda x: x[2]!=None and x[3] !=None)\
    # .saveAsTextFile("hdfs:/user/s1557920/amazonresults_"+str(time.time()))
    # .saveAsTextFile("file:///user/s1557920/amazonresults_sample.txt")

from pyspark.sql.types import StringType, StructField, StructType, BooleanType, ArrayType, IntegerType, FloatType

schema = StructType([
        StructField("asin", StringType(), True),
        StructField("imUrl", StringType(), True),
        StructField("colours", ArrayType(
            StructType([
                StructField("r", IntegerType(), True),
                StructField("g", IntegerType(), True),
                StructField("b", IntegerType(), True),
                StructField("name", StringType(), True),
                StructField("proportion", FloatType(), True)
            ])
        ), True),
        StructField("category", StringType(), True),
        StructField("salesrank", IntegerType(), True),
        StructField("price", FloatType(), True)
])

df2 = sqlc.createDataFrame(useful_information, schema)
df2.write.option("compression", "gzip").json("hdfs:/user/s1557920/amazonresults_all_json_"+str(time.time()))

# print useful_information.take(1)
# /user/s1557920/amazonresults_json_1516010161.33