# sc = SparkContext(appName="Amazon colours")
# sc = SparkContext("local", "project")
sqlc = SQLContext(sc)
path = "hdfs:/user/s1557920/amazonresults_json_1516016151.15"
rdd1 = sc.wholeTextFiles(path)
df = sqlc.read.json(rdd1)

from pyspark.sql.types import StringType, StructField, StructType, BooleanType, ArrayType, IntegerType, FloatType

schema = StructType([
        StructField("asin", StringType(), True),
        StructField("imUrl", StringType(), True),
        StructField("colours", ArrayType(
            StructType([
                StructField("r", IntegerType(), True),
                StructField("g", IntegerType(), True),
                StructField("b", IntegerType(), True),
                StructField("name", StringType(), True),
                StructField("proportion", FloatType(), True)
            ])
        ), True),
        StructField("price", FloatType(), True)
])

schema_dict = {'fields': [
    {'metadata': {}, 'name': 'asin', 'nullable': True, 'type': 'string'},
    {'metadata': {}, 'name': 'imUrl', 'nullable': True, 'type': 'string'},
    {'metadata': {}, 'name': 'rgd', 'nullable': True, 'type': 'ArrayType'}
], 'type': 'struct'}


rdd.flatMap(lambda x )