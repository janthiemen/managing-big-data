### Check for salesRank
path = "/data/doina/UCSD-Amazon-Data/meta_Electronics.json.gz"
sc = SparkContext("local", "project")
sqlc = SQLContext(sc)
df = sqlc.read.json(path)

#Count the total amount of salesRanks
rdd1 = df.select("salesRank").rdd
rdd2 = rdd1.filter(lambda x: x["salesRank"] != None)
rdd3 = rdd2.flatMap(lambda x: [i for i in x["salesRank"]])
rdd4 = rdd3.filter(lambda x: x != None)
rdd4.count()

#Count the amount of salesRanks that have the category Electronics
rdd5 = rdd2.map(lambda x: x["salesRank"]["Electronics"])
rdd6 = rdd5.filter(lambda x: x != None)
rdd6.count()
#They differ from each other, meaning some Electronics items have a
#salesRank in a different category

### Get useful information
useful_information = df.select("asin","imUrl","salesRank").rdd\
    .filter(lambda x: x["salesRank"] != None and x["imUrl"] != None)\
    .map(lambda x: (x["asin"], x["imUrl"], next(iter([(i, x["salesRank"][i]) for i in x["salesRank"].asDict() if x["salesRank"][i] != None] or []), None)))\
    .filter(lambda x : x[2] != None)


#output is: (asin, imUrl, (salesRank Category, salesRank))

