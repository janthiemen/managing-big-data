from pyspark import SparkContext
from pyspark import SQLContext
import colorgramext
import urllib2
import getColours
import os

def getPillow(x):
    os.system("pip install --user pillow")
    from PIL import Image
    return x


def extractrgb(url):
    img = Image.open(urllib2.urlopen(url))
    colours = colorgramext.extract_pillow(img, 2)
    return [(colour.rgb, getColours.get_colour_name((colour.rgb.r, colour.rgb.g, colour.rgb.b)), colour.proportion) for colour in colours]

path = "/data/doina/UCSD-Amazon-Data/meta_Electronics.json.gz"
sc = SparkContext(appName="Amazon colours")
sqlc = SQLContext(sc)
path = "/user/s1557920/amazonresults_json_1516010161.33"
df = sqlContext.read.json(path)
df = sqlc.read.json(path)

useful_information = df.select("asin","imUrl","salesRank").rdd\
    .filter(lambda x: x["salesRank"] != None and x["imUrl"] != None)\
    .map(lambda x: (x["asin"], x["imUrl"], extractrgb(getPillow(x)["imUrl"]),next(iter([(i, x["salesRank"][i]) for i in x["salesRank"].asDict() if x["salesRank"][i] != None] or []), None)))\
    .saveAsTextFile("/home/s1557920/amazonresults.txt")

print useful_information.take(1)
