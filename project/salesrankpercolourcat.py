CSS3_NAMES_TO_GROUPS = {
    u'aliceblue': u'blue',
    u'antiquewhite': u'pink',
    u'aqua': u'blue',
    u'aquamarine': u'blue',
    u'azure': u'blue',
    u'beige': u'white',
    u'bisque': u'pink',
    u'black': u'black',
    u'blanchedalmond': u'pink',
    u'blue': u'blue',
    u'blueviolet': u'purple',
    u'brown': u'brown',
    u'burlywood': u'brown',
    u'cadetblue': u'blue',
    u'chartreuse': u'green',
    u'chocolate': u'orange',
    u'coral': u'orange',
    u'cornflowerblue': u'blue',
    u'cornsilk': u'white',
    u'crimson': u'red',
    u'cyan': u'blue',
    u'darkblue': u'blue',
    u'darkcyan': u'blue',
    u'darkgoldenrod': u'brown',
    u'darkgray': u'gray',
    u'darkgrey': u'gray',
    u'darkgreen': u'green',
    u'darkkhaki': u'brown',
    u'darkmagenta': u'purple',
    u'darkolivegreen': u'green',
    u'darkorange': u'orange',
    u'darkorchid': u'purple',
    u'darkred': u'red',
    u'darksalmon': u'pink',
    u'darkseagreen': u'green',
    u'darkslateblue': u'purple',
    u'darkslategray': u'gray',
    u'darkslategrey': u'gray',
    u'darkturquoise': u'blue',
    u'darkviolet': u'purple',
    u'deeppink': u'pink',
    u'deepskyblue': u'blue',
    u'dimgray': u'gray',
    u'dimgrey': u'gray',
    u'dodgerblue': u'blue',
    u'firebrick': u'red',
    u'floralwhite': u'white',
    u'forestgreen': u'green',
    u'fuchsia': u'purple',
    u'gainsboro': u'gray',
    u'ghostwhite': u'white',
    u'gold': u'yellow',
    u'goldenrod': u'yellow',
    u'gray': u'gray',
    u'grey': u'gray',
    u'green': u'green',
    u'greenyellow': u'green',
    u'honeydew': u'blue',
    u'hotpink': u'pink',
    u'indianred': u'red',
    u'indigo': u'purple',
    u'ivory': u'white',
    u'khaki': u'yellow',
    u'lavender': u'gray',
    u'lavenderblush': u'pink',
    u'lawngreen': u'green',
    u'lemonchiffon': u'yellow',
    u'lightblue': u'blue',
    u'lightcoral': u'pink',
    u'lightcyan': u'blue',
    u'lightgoldenrodyellow': u'yellow',
    u'lightgray': u'gray',
    u'lightgrey': u'gray',
    u'lightgreen': u'green',
    u'lightpink': u'pink',
    u'lightsalmon': u'pink',
    u'lightseagreen': u'blue',
    u'lightskyblue': u'blue',
    u'lightslategray': u'gray',
    u'lightslategrey': u'gray',
    u'lightsteelblue': u'blue',
    u'lightyellow': u'yellow',
    u'lime': u'green',
    u'limegreen': u'green',
    u'linen': u'pink',
    u'magenta': u'purple',
    u'maroon': u'red',
    u'mediumaquamarine': u'blue',
    u'mediumblue': u'blue',
    u'mediumorchid': u'purple',
    u'mediumpurple': u'purple',
    u'mediumseagreen': u'green',
    u'mediumslateblue': u'blue',
    u'mediumspringgreen': u'green',
    u'mediumturquoise': u'blue',
    u'mediumvioletred': u'purple',
    u'midnightblue': u'blue',
    u'mintcream': u'white',
    u'mistyrose': u'pink',
    u'moccasin': u'pink',
    u'navajowhite': u'pink',
    u'navy': u'blue',
    u'oldlace': u'pink',
    u'olive': u'green',
    u'olivedrab': u'green',
    u'orange': u'orange',
    u'orangered': u'red',
    u'orchid': u'purple',
    u'palegoldenrod': u'brown',
    u'palegreen': u'green',
    u'paleturquoise': u'blue',
    u'palevioletred': u'pink',
    u'papayawhip': u'pink',
    u'peachpuff': u'pink',
    u'peru': u'brown',
    u'pink': u'pink',
    u'plum': u'purple',
    u'powderblue': u'blue',
    u'purple': u'purple',
    u'red': u'red',
    u'rosybrown': u'purple',
    u'royalblue': u'blue',
    u'saddlebrown': u'brown',
    u'salmon': u'pink',
    u'sandybrown': u'brown',
    u'seagreen': u'green',
    u'seashell': u'pink',
    u'sienna': u'brown',
    u'silver': u'gray',
    u'skyblue': u'blue',
    u'slateblue': u'blue',
    u'slategray': u'gray',
    u'slategrey': u'gray',
    u'snow': u'white',
    u'springgreen': u'green',
    u'steelblue': u'blue',
    u'tan': u'brown',
    u'teal': u'blue',
    u'thistle': u'purple',
    u'tomato': u'red',
    u'turquoise': u'blue',
    u'violet': u'purple',
    u'wheat': u'pink',
    u'white': u'white',
    u'whitesmoke': u'white',
    u'yellow': u'yellow',
    u'yellowgreen': u'green',
}

path = "file:///home/s1557920/allresults"
df = sqlContext.read.json(path)

most_dominant_colours = df.select("colours", "category", "salesrank")\
    .map(lambda x: (x["category"]+" "+CSS3_NAMES_TO_GROUPS[x["colours"][0]["name"]], x["salesrank"]))

import pickle
with open('/home/s1557920/pickles/most_dominant_colours_avg_median.pickle', 'wb') as handle:
    pickle.dump(most_dominant_colours.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)
