from pyspark import SparkContext
from pyspark import SQLContext
import numpy
import pickle

#Our custom made color mapping, mapping 140 HTML colors to 11 colors of our choice.
CSS3_NAMES_TO_GROUPS = {
    u'aliceblue': u'blue',
    u'antiquewhite': u'pink',
    u'aqua': u'blue',
    u'aquamarine': u'blue',
    u'azure': u'blue',
    u'beige': u'white',
    u'bisque': u'pink',
    u'black': u'black',
    u'blanchedalmond': u'pink',
    u'blue': u'blue',
    u'blueviolet': u'purple',
    u'brown': u'brown',
    u'burlywood': u'brown',
    u'cadetblue': u'blue',
    u'chartreuse': u'green',
    u'chocolate': u'orange',
    u'coral': u'orange',
    u'cornflowerblue': u'blue',
    u'cornsilk': u'white',
    u'crimson': u'red',
    u'cyan': u'blue',
    u'darkblue': u'blue',
    u'darkcyan': u'blue',
    u'darkgoldenrod': u'brown',
    u'darkgray': u'gray',
    u'darkgrey': u'gray',
    u'darkgreen': u'green',
    u'darkkhaki': u'brown',
    u'darkmagenta': u'purple',
    u'darkolivegreen': u'green',
    u'darkorange': u'orange',
    u'darkorchid': u'purple',
    u'darkred': u'red',
    u'darksalmon': u'pink',
    u'darkseagreen': u'green',
    u'darkslateblue': u'purple',
    u'darkslategray': u'gray',
    u'darkslategrey': u'gray',
    u'darkturquoise': u'blue',
    u'darkviolet': u'purple',
    u'deeppink': u'pink',
    u'deepskyblue': u'blue',
    u'dimgray': u'gray',
    u'dimgrey': u'gray',
    u'dodgerblue': u'blue',
    u'firebrick': u'red',
    u'floralwhite': u'white',
    u'forestgreen': u'green',
    u'fuchsia': u'purple',
    u'gainsboro': u'gray',
    u'ghostwhite': u'white',
    u'gold': u'yellow',
    u'goldenrod': u'yellow',
    u'gray': u'gray',
    u'grey': u'gray',
    u'green': u'green',
    u'greenyellow': u'green',
    u'honeydew': u'blue',
    u'hotpink': u'pink',
    u'indianred': u'red',
    u'indigo': u'purple',
    u'ivory': u'white',
    u'khaki': u'yellow',
    u'lavender': u'gray',
    u'lavenderblush': u'pink',
    u'lawngreen': u'green',
    u'lemonchiffon': u'yellow',
    u'lightblue': u'blue',
    u'lightcoral': u'pink',
    u'lightcyan': u'blue',
    u'lightgoldenrodyellow': u'yellow',
    u'lightgray': u'gray',
    u'lightgrey': u'gray',
    u'lightgreen': u'green',
    u'lightpink': u'pink',
    u'lightsalmon': u'pink',
    u'lightseagreen': u'blue',
    u'lightskyblue': u'blue',
    u'lightslategray': u'gray',
    u'lightslategrey': u'gray',
    u'lightsteelblue': u'blue',
    u'lightyellow': u'yellow',
    u'lime': u'green',
    u'limegreen': u'green',
    u'linen': u'pink',
    u'magenta': u'purple',
    u'maroon': u'red',
    u'mediumaquamarine': u'blue',
    u'mediumblue': u'blue',
    u'mediumorchid': u'purple',
    u'mediumpurple': u'purple',
    u'mediumseagreen': u'green',
    u'mediumslateblue': u'blue',
    u'mediumspringgreen': u'green',
    u'mediumturquoise': u'blue',
    u'mediumvioletred': u'purple',
    u'midnightblue': u'blue',
    u'mintcream': u'white',
    u'mistyrose': u'pink',
    u'moccasin': u'pink',
    u'navajowhite': u'pink',
    u'navy': u'blue',
    u'oldlace': u'pink',
    u'olive': u'green',
    u'olivedrab': u'green',
    u'orange': u'orange',
    u'orangered': u'red',
    u'orchid': u'purple',
    u'palegoldenrod': u'brown',
    u'palegreen': u'green',
    u'paleturquoise': u'blue',
    u'palevioletred': u'pink',
    u'papayawhip': u'pink',
    u'peachpuff': u'pink',
    u'peru': u'brown',
    u'pink': u'pink',
    u'plum': u'purple',
    u'powderblue': u'blue',
    u'purple': u'purple',
    u'red': u'red',
    u'rosybrown': u'purple',
    u'royalblue': u'blue',
    u'saddlebrown': u'brown',
    u'salmon': u'pink',
    u'sandybrown': u'brown',
    u'seagreen': u'green',
    u'seashell': u'pink',
    u'sienna': u'brown',
    u'silver': u'gray',
    u'skyblue': u'blue',
    u'slateblue': u'blue',
    u'slategray': u'gray',
    u'slategrey': u'gray',
    u'snow': u'white',
    u'springgreen': u'green',
    u'steelblue': u'blue',
    u'tan': u'brown',
    u'teal': u'blue',
    u'thistle': u'purple',
    u'tomato': u'red',
    u'turquoise': u'blue',
    u'violet': u'purple',
    u'wheat': u'pink',
    u'white': u'white',
    u'whitesmoke': u'white',
    u'yellow': u'yellow',
    u'yellowgreen': u'green',
}

"""
This script gets the data for plots from the dataset generated by "downloadimage.py" and saves those to pickles
"""

path = "/user/s1557920/amazonresults_all_json_1516037263.48"
sc = SparkContext(appName="Amazon colours")
sqlc = SQLContext(sc)
df = sqlc.read.json(path)

#First create the datasets
most_dominant_color = df.select("colours", "category", "salesrank")\
    .map(lambda x: (x["category"]+" "+x["colours"][0]["name"], x["salesrank"]))
most_dominant_colors = df.select("colours", "category", "salesrank")\
    .map(lambda x: (x["category"]+" "+x["colours"][0]["name"]+" "+(x["colours"][1]["name"] if len(x["colours"]) > 1 else ""), x["salesrank"]))

most_dominant_color_mapped = df.select("colours", "category", "salesrank")\
    .map(lambda x: (x["category"]+" "+CSS3_NAMES_TO_GROUPS[x["colours"][0]["name"]], x["salesrank"]))
most_dominant_colors_mapped = df.select("colours", "category", "salesrank")\
    .map(lambda x: (x["category"]+" "+CSS3_NAMES_TO_GROUPS[x["colours"][0]["name"]]+" "+(CSS3_NAMES_TO_GROUPS[x["colours"][1]["name"]] if len(x["colours"]) > 1 else ""), x["salesrank"]))


#MOST DOMINANT COLOR
#Combine the category and most dominant color as a key and calculate the average SalesRank
most_dominant_color_avg = most_dominant_color\
    .aggregateByKey((0,0), lambda a,b: (a[0] + b, a[1] + 1), lambda a,b: (a[0] + b[0], a[1] + b[1]))\
    .mapValues(lambda v: (v[0]/v[1],v[1]))

with open('/home/s1557920/pickles/most_dominant_color_avg.pickle', 'wb') as handle:
    pickle.dump(most_dominant_color_avg.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)

#Combine the category and most dominant color as a key and calculate the median SalesRank
most_dominant_color_median = most_dominant_color\
    .groupByKey().mapValues(list)\
    .map(lambda x: (x[0],numpy.median(x[1]),len(x[1])))

with open('/home/s1557920/pickles/most_dominant_color_median.pickle', 'wb') as handle:
    pickle.dump(most_dominant_color_median.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)


#TWO MOST DOMINANT COLORS
#Average
most_dominant_colors_avg = most_dominant_colors\
    .aggregateByKey((0,0), lambda a,b: (a[0] + b, a[1] + 1), lambda a,b: (a[0] + b[0], a[1] + b[1]))\
    .mapValues(lambda v: (v[0]/v[1],v[1]))

with open('/home/s1557920/pickles/most_dominant_colors_avg.pickle', 'wb') as handle:
    pickle.dump(most_dominant_colors_avg.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)

#Median
most_dominant_colors_median = most_dominant_colors\
    .groupByKey().mapValues(list)\
    .map(lambda x: (x[0],numpy.median(x[1]),len(x[1])))

with open('/home/s1557920/pickles/most_dominant_colors_median.pickle', 'wb') as handle:
    pickle.dump(most_dominant_colors_median.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)



##### MAPPED COLOURS #####

#MOST DOMINANT COLOR
#Average
most_dominant_color_avg_mapped = most_dominant_color_mapped\
    .aggregateByKey((0,0), lambda a,b: (a[0] + b, a[1] + 1), lambda a,b: (a[0] + b[0], a[1] + b[1])) \
    .mapValues(lambda v: (v[0] / v[1], v[1]))

with open('/home/s1557920/pickles/most_dominant_color_avg_mapped.pickle', 'wb') as handle:
    pickle.dump(most_dominant_color_avg_mapped.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)

#Median
most_dominant_color_median_mapped = most_dominant_color_mapped\
    .groupByKey().mapValues(list)\
    .map(lambda x: (x[0],numpy.median(x[1]),len(x[1])))

with open('/home/s1557920/pickles/most_dominant_color_median_mapped.pickle', 'wb') as handle:
    pickle.dump(most_dominant_color_median_mapped.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)


#TWO MOST DOMINANT COLORS
#Average
most_dominant_colors_avg_mapped = most_dominant_colors_mapped\
    .aggregateByKey((0,0), lambda a,b: (a[0] + b, a[1] + 1), lambda a,b: (a[0] + b[0], a[1] + b[1])) \
    .mapValues(lambda v: (v[0] / v[1], v[1]))

with open('/home/s1557920/pickles/most_dominant_colors_avg_mapped.pickle', 'wb') as handle:
    pickle.dump(most_dominant_colors_avg_mapped.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)

#Median
most_dominant_colors_median_mapped = most_dominant_colors_mapped\
    .groupByKey().mapValues(list)\
    .map(lambda x: (x[0],numpy.median(x[1]),len(x[1])))

with open('/home/s1557920/pickles/most_dominant_colors_median_mapped.pickle', 'wb') as handle:
    pickle.dump(most_dominant_colors_median_mapped.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)


##### BOXPLOT DATA #####
#MOST DOMINANT COLOR

#Output = Category, Median, 25perc, 75perc, min, max, count
most_dominant_color_boxplot = most_dominant_color\
    .groupByKey().mapValues(list)\
    .map(lambda x: (x[0],numpy.median(x[1]),numpy.percentile(x[1],25),numpy.percentile(x[1],75),min(x[1]),max(x[1]),len(x[1])))

with open('/home/s1557920/pickles/most_dominant_color_boxplot.pickle', 'wb') as handle:
    pickle.dump(most_dominant_color_boxplot.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)

#TWO MOST DOMINANT COLORS
most_dominant_colors_boxplot = most_dominant_colors\
    .groupByKey().mapValues(list) \
    .map(lambda x: (x[0], numpy.median(x[1]), numpy.percentile(x[1], 25), numpy.percentile(x[1], 75), min(x[1]), max(x[1]), len(x[1])))

with open('/home/s1557920/pickles/most_dominant_colors_boxplot.pickle', 'wb') as handle:
    pickle.dump(most_dominant_colors_boxplot.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)


#MOST DOMINANT COLOR MAPPED

#Output = Category, Median, 25perc, 75perc, min, max, count
most_dominant_color_boxplot_mapped = most_dominant_color_mapped\
    .groupByKey().mapValues(list)\
    .map(lambda x: (x[0],numpy.median(x[1]),numpy.percentile(x[1],25),numpy.percentile(x[1],75),min(x[1]),max(x[1]),len(x[1])))

with open('/home/s1557920/pickles/most_dominant_color_boxplot_mapped.pickle', 'wb') as handle:
    pickle.dump(most_dominant_color_boxplot_mapped.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)

#TWO MOST DOMINANT COLORS
most_dominant_colors_boxplot_mapped = most_dominant_colors_mapped\
    .groupByKey().mapValues(list) \
    .map(lambda x: (x[0], numpy.median(x[1]), numpy.percentile(x[1], 25), numpy.percentile(x[1], 75), min(x[1]), max(x[1]), len(x[1])))

with open('/home/s1557920/pickles/most_dominant_colors_boxplot_mapped.pickle', 'wb') as handle:
    pickle.dump(most_dominant_colors_boxplot_mapped.collect(), handle, protocol=pickle.HIGHEST_PROTOCOL)
