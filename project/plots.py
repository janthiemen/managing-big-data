from pylab import rcParams
import matplotlib.pyplot as plt
import pickle
import getColours
import numpy as np

NUMBARS = 11
FOLDER = 'plots_median_mapped'
CATEGORIES = [
    "Cell Phones & Accessories",
    "Clothing",
    "Electronics",
    "Home &amp; Kitchen",
    "Jewelry",
    "Kitchen & Dining",
    "Shoes",
    "Sports &amp; Outdoors",
    "Toys & Games",
    "Watches",
]


def load_obj(path):
    with open(path, 'rb') as f:
        return pickle.load(f)

#Plot the salesRank average or median for the most dominant color
def plotMostDominantColor(reslist, filename):
    rcParams['figure.figsize'] = 30, 18
    if len(reslist[0]) == 2:
        #This one is a tuple, so split it up
        reslist = [(reslist[i][0], reslist[i][1][0], reslist[i][1][1]) for i in range(len(reslist))]

    reslist.sort(key=lambda tup: tup[1])
    colors = []
    x = []
    y = []
    count = []
    labels = []
    for tup in reslist:
        splitted = tup[0].split()
        x.append(str(splitted[-1]))
        y.append(int(tup[1]))
        count.append(tup[2])
        labels.append(str(splitted[-1]) + ": " + str(tup[2]))
    x = x[:NUMBARS]
    y = y[:NUMBARS]
    labels = labels[:NUMBARS]
    for color in x:
        colors.append(str(getColours.CSS3_NAMES_TO_HEX[color]))

    # for s in colors:
    #     if isinstance(s, str):
    #         print "AAAAAAAAAAAAAAAAAAA ordinary string"
    #     elif isinstance(s, unicode):
    #         print "AAAAAAAAAAAAAAAAAAA unicode string"
    #     else:
    #         print "CCCCCCCCCCCCCCCCCCC not a string ...."

    fig, ax = plt.subplots()
    ax.bar(range(len(x)),y,color=colors, tick_label=labels)
    ax.set_facecolor("#e8e8e8")
    ax.set_xlabel('Colour - occurrences', size=40)
    ax.set_ylabel('Median salesrank', size=40)
    plt.tick_params(axis='both' , which="major", labelsize=23)
    plt.savefig(FOLDER+"/"+filename+'.png')
    plt.clf()

#Plot the salesRank average or median for the two most dominant colors
def plotMostDominantColors(reslist, filename):
    rcParams['figure.figsize'] = 30, 18
    if len(reslist[0]) == 2:
        #This one is a tuple, so split it up
        reslist = [(reslist[i][0], reslist[i][1][0], reslist[i][1][1]) for i in range(len(reslist))]

    reslist.sort(key=lambda tup: tup[1])
    colors = []
    x = []
    y = []
    #count = []
    for tup in reslist:
        if tup[2] > 1:
            splitted = tup[0].split()
            # print tup[2]
            x.append(splitted[-1]+"-"+(splitted[-2] if splitted[-2] in getColours.CSS3_NAMES_TO_HEX else "")+"-"+str(tup[2]))
            y.append(tup[1])
            #count.append(tup[1][1])
    x = x[:NUMBARS]
    y = y[:NUMBARS]
    for color in x:
        a, b, c = color.split("-")
        colors.append(getColours.CSS3_NAMES_TO_HEX[a])
        if b in getColours.CSS3_NAMES_TO_HEX:
            colors.append(getColours.CSS3_NAMES_TO_HEX[b])

    ind = np.arange(len(y))  # the x locations for the groups
    width = 0.35       # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(ind, y, width, color=[colors[i] for i in range(min(len(colors), 100)) if i % 2 == 0])
    rects2 = ax.bar(ind + width, y, width, color=[colors[i] for i in range(min(len(colors), 100)) if i % 2 != 0])
    ax.set_xlabel('Colour - occurrences', size=20)
    ax.set_ylabel('Median salesrank', size=20)
    ax.set_title(filename)
    ax.set_xticks(ind + width)
    ax.set_xticklabels(x)
    ax.set_facecolor("#e8e8e8")
    ax.tick_params(axis='both', which="major", labelsize=23)
    plt.savefig(FOLDER+"/"+filename+'.png')
    plt.clf()

#Plot the salesRank average or median for the most dominant color
def plotMostDominantColorCat(reslist, filename):
    rcParams['figure.figsize'] = 30, 18
    if len(reslist[0]) == 2:
        #This one is a tuple, so split it up
        reslist = [(reslist[i][0], reslist[i][1][0], reslist[i][1][1]) for i in range(len(reslist))]

    reslistcat = {}
    for res in reslist:
        cat = " ".join([word for word in res[0].split() if word not in getColours.CSS3_NAMES_TO_HEX])
        if not cat in reslistcat:
            reslistcat[cat] = []
        reslistcat[cat].append(res)

    for cat in [key for key in reslistcat.keys() if key in CATEGORIES]:
        reslist = reslistcat[cat]
        reslist.sort(key=lambda tup: tup[1])
        colors = []
        x = []
        y = []
        count = []
        labels = []
        for tup in reslist:
            splitted = tup[0].split()
            x.append(splitted[-1])
            y.append(tup[1])
            count.append(tup[2])
            labels.append(str(splitted[-1]) + ": " + str(tup[2]))
        x = x[:NUMBARS]
        y = y[:NUMBARS]
        labels = labels[:NUMBARS]
        for color in x:
            colors.append(getColours.CSS3_NAMES_TO_HEX[color])
        fig, ax = plt.subplots()
        ax.bar(range(len(x)),y,color=colors, tick_label=labels)
        ax.set_xlabel('Colour - occurrences', size=20)
        ax.set_ylabel('Median salesrank', size=20)
        ax.set_title(cat + ' ' + filename)
        ax.set_facecolor("#e8e8e8")
        ax.tick_params(axis='both', which="major", labelsize=23)
        plt.savefig(FOLDER+"/"+cat+"_"+filename+'.png')
        plt.clf()


#Plot the salesRank average or median for the two most dominant colors
def plotMostDominantColorsCat(reslist, filename):
    rcParams['figure.figsize'] = 30, 18
    if len(reslist[0]) == 2:
        #This one is a tuple, so split it up
        reslist = [(reslist[i][0], reslist[i][1][0], reslist[i][1][1]) for i in range(len(reslist))]

    reslistcat = {}
    for res in reslist:
        cat = " ".join([word for word in res[0].split() if word not in getColours.CSS3_NAMES_TO_HEX])
        if not cat in reslistcat:
            reslistcat[cat] = []
        reslistcat[cat].append(res)

    for cat in [key for key in reslistcat.keys() if key in CATEGORIES]:
        reslist = reslistcat[cat]
        reslist.sort(key=lambda tup: tup[1])
        colors = []
        x = []
        y = []
        #count = []
        for tup in reslist:
            if tup[2] > 1:
                splitted = tup[0].split()
                x.append(splitted[-1]+"-"+(splitted[-2] if splitted[-2] in getColours.CSS3_NAMES_TO_HEX else "")+"-"+str(tup[2]))
                y.append(tup[1])
                #count.append(tup[1][1])
        x = x[:NUMBARS]
        y = y[:NUMBARS]
        for color in x:
            a, b, c = color.split("-")
            colors.append(getColours.CSS3_NAMES_TO_HEX[a])
            if b in getColours.CSS3_NAMES_TO_HEX:
                colors.append(getColours.CSS3_NAMES_TO_HEX[b])
            else:
                colors.append(getColours.CSS3_NAMES_TO_HEX["white"])

        ind = np.arange(len(y))  # the x locations for the groups
        width = 0.35       # the width of the bars

        fig, ax = plt.subplots()
        rects1 = ax.bar(ind, y, width, color=[colors[i] for i in range(len(colors)) if i % 2 == 0])
        rects2 = ax.bar(ind + width, y, width, color=[colors[i] for i in range(len(colors)) if i % 2 != 0])
        ax.set_xlabel('Colour - occurrences', size=20)
        ax.set_ylabel('Median salesrank', size=20)
        ax.set_title(cat + ' ' + filename)
        ax.set_xticks(ind + width)
        ax.set_xticklabels(x)
        ax.tick_params(axis='both', which="major", labelsize=23)
        ax.set_facecolor("#e8e8e8")
        plt.savefig(FOLDER+"/"+cat+"_"+filename+'.png')
        plt.clf()

files_one = [
    # "most_dominant_color_avg",
    #"most_dominant_color_avg_mapped",
    # "most_dominant_color_avg_mapped_khaki",
    # "most_dominant_color_median",
    "most_dominant_color_median_mapped"
    # "only_men_150",
    # "only_men_80_150",
    # "only_men_80",
    # "only_men_all",
    # "only_women_150",
    # "only_women_80_150",
    # "only_women_80",
    # "only_women_all",
]

files_two = [
    # "most_dominant_colors_avg",
    #"most_dominant_colors_avg_mapped",
    # "most_dominant_color_avg_mapped_khaki",
    # "most_dominant_colors_median",
    "most_dominant_colors_median_mapped"
]

# for file in files_one:
#     plotMostDominantColor(load_obj("pickles/"+file+".pickle"), file)
#
# for file in files_two:
#     plotMostDominantColors(load_obj("pickles/"+file+".pickle"), file)

for file in files_one:
    plotMostDominantColorCat(load_obj("pickles/"+file+".pickle"), file)

for file in files_two:
    plotMostDominantColorsCat(load_obj("pickles/"+file+".pickle"), file)
#
# values = {
#     "0-10": [(u'pink', (402008, 28466)), (u'black', (458107, 30938)), (u'orange', (427349, 710)), (u'white', (376922, 374543)), (u'red', (435829, 5026)), (u'purple', (422874, 9956)), (u'gray', (409586, 48492)), (u'green', (434279, 5001)), (u'brown', (451145, 9608)), (u'yellow', (407334, 1970)), (u'blue', (407137, 11443))],
#     "10-100": [(u'pink', (439939, 53746)), (u'black', (537136, 48013)), (u'orange', (449771, 903)), (u'white', (358683, 762268)), (u'red', (512868, 7363)), (u'purple', (547358, 16560)), (u'gray', (489086, 95187)), (u'green', (547990, 8469)), (u'brown', (548087, 21059)), (u'yellow', (456217, 2361)), (u'blue', (458716, 17093))],
#     "gt100": [(u'pink', (460235, 8105)), (u'black', (581040, 7211)), (u'orange', (680372, 56)), (u'white', (398765, 114106)), (u'red', (824696, 595)), (u'purple', (875137, 2727)), (u'gray', (706995, 14919)), (u'green', (780461, 1258)), (u'brown', (788562, 3126)), (u'yellow', (445225, 164)), (u'blue', (561928, 1255))]
# }
#
# for key in values.keys():
#     data = values[key]
#     plotMostDominantColor(data, "salesranksbyprice_"+key)
#

"""
Clothing
Sports & outdoors
Sohes
Cell phones and
Toys and games
Home and kitchen
Jewelery
Kitchen and dining
Watches
Electronics

colour_rank = df1.select("colours", "category", "salesrank")\
    .map(lambda x: (x["colours"][0]["name"], x["salesrank"]))\
    .toDF(["name", "rank"])

from pyspark.ml.feature import VectorAssembler
assembler_name = VectorAssembler(
  inputCols=["name"], outputCol="name"
)
assembled_name = assembler_name.transform(colour_rank)

assembler_rank = VectorAssembler(
  inputCols=["name"], outputCol="name"
)

assembled_rank = assembler_rank.transform(colour_rank)

def getIfromRGB(r,g,b):
    return (r<<16) + (g<<8) + b

categories = [
    "Cell Phones & Accessories",
    "Clothing",
    "Electronics",
    "Home &amp; Kitchen",
    "Jewelry",
    "Kitchen & Dining",
    "Shoes",
    "Sports &amp; Outdoors",
    "Toys & Games",
    "Watches",
]

cat_correlations = {}

for category in categories:
    cat_correlations[category] = df1.select("colours", "category", "salesrank").filter(df1.category == category)\
        .map(lambda x: ((x["colours"][0]["r"]<<16) + (x["colours"][0]["g"]<<8) + x["colours"][0]["b"], x["salesrank"]))\
        .toDF(["colour", "rank"]).stat.corr("colour", "rank")

df1.stat.corr("colour", "rank")"""

# women_shoes = {
# "women_all" : [(u'gray', (126549, 1388)), (u'black', (115842, 637)), (u'red', (139424, 55)), (u'brown', (135372, 251)), (u'orange', (259710, 3)), (u'pink', (115909, 1703)), (u'green', (134552, 82)), (u'white', (139601, 42435)), (u'yellow', (74472, 13)), (u'blue', (124555, 40)), (u'purple', (116632, 107))],
# "women_all_nowhite" : [(u'gray', (135646, 20499)), (u'black', (146419, 9097)), (u'red', (138599, 1220)), (u'brown', (144069, 5487)), (u'orange', (167775, 63)), (u'pink', (122006, 2589)), (u'green', (135485, 1957)), (u'white', (150524, 678)), (u'yellow', (103457, 69)), (u'blue', (120826, 916)), (u'purple', (135071, 4139))],
# "women_0_80" : [(u'gray', (124801, 1081)), (u'black', (108391, 472)), (u'red', (121824, 43)), (u'brown', (127229, 195)), (u'orange', (342421, 2)), (u'pink', (110495, 1350)), (u'green', (152636, 52)), (u'white', (134347, 31419)), (u'yellow', (106613, 8)), (u'blue', (119780, 39)), (u'purple', (115897, 83))],
# "women_80_150": [(u'gray', (130265, 210)), (u'black', (146373, 95)), (u'red', (254861, 7)), (u'brown', (155503, 45)), (u'orange', (94288, 1)), (u'pink', (127584, 257)), (u'green', (109349, 24)), (u'white', (147006, 7788)), (u'yellow', (23047, 5)), (u'purple', (105890, 19))],
# "women_150plus": [(u'gray', (137987, 97)), (u'black', (124647, 70)), (u'red', (129178, 5)), (u'brown', (197387, 11)), (u'pink', (160795, 96)), (u'green', (78639, 6)), (u'white', (172869, 3228)), (u'blue', (310768, 1)), (u'purple', (169643, 5))],
# "men_all": [(u'gray', (122873, 709)), (u'black', (111092, 364)), (u'red', (86189, 26)), (u'brown', (110221, 118)), (u'orange', (180444, 1)), (u'pink', (111338, 193)), (u'green', (130734, 69)), (u'white', (125746, 14695)), (u'yellow', (287525, 2)), (u'blue', (128876, 10)), (u'purple', (91840, 49))],
# "men_0_80":[(u'gray', (125885, 445)), (u'black', (102082, 249)), (u'red', (53021, 17)), (u'brown', (116259, 64)), (u'orange', (180444, 1)), (u'pink', (109912, 130)), (u'green', (98570, 29)), (u'white', (115908, 8709)), (u'yellow', (287525, 2)), (u'blue', (133439, 9)), (u'purple', (69886, 21))],
# "men_80_150": [(u'gray', (110676, 215)), (u'black', (140663, 67)), (u'red', (86935, 6)), (u'brown', (88569, 38)), (u'pink', (116117, 40)), (u'green', (170747, 17)), (u'white', (132922, 4271)), (u'blue', (87811, 1)), (u'purple', (89808, 22))],
# "men_150plus": [(u'gray', (149043, 49)), (u'black', (116556, 48)), (u'red', (272649, 3)), (u'brown', (137491, 16)), (u'pink', (111083, 23)), (u'green', (141714, 23)), (u'white', (157834, 1715)), (u'purple', (176124, 6))],
# "men_all_nowhite_": [(u'gray', (124329, 9249)), (u'black', (127838, 3236)), (u'red', (107306, 231)), (u'brown', (138490, 1199)), (u'orange', (97411, 38)), (u'pink', (117935, 285)), (u'green', (130353, 793)), (u'white', (116995, 180)), (u'yellow', (100355, 40)), (u'blue', (108006, 284)), (u'purple', (109586, 701))]
# }
#
# for key in women_shoes.keys():
#     data = women_shoes[key]
#     plotMostDominantColor(data, "shoes_"+key)
#
#
