from pyspark import SparkContext
from pyspark import SQLContext
from pyspark.sql.types import BooleanType
from pyspark.sql.functions import udf
from pyspark.sql.functions import *
import pickle

import numpy

# Filter functions
def findWomen(arr):
    """
    Check if "Women" occurs in a list of lists
    :param arr: a list of lists
    :return: bool
    """
    for ar in arr:
        if "Women" in ar:
            return True
    return False

def findMen(arr):
    """
    Check if "Men" occurs in a list of lists
    :param arr: a list of lists
    :return: bool
    """
    for ar in arr:
        if "Men" in ar:
            return True
    return False

def savetopickle(data, filename):
    """
    Function to save an RDD to a pickle.
    :param data: The RDD
    :param filename: The filename of the pickle
    :return:
    """
    with open('/home/s1557920/pickles/'+filename+'.pickle', 'wb') as f:
        pickle.dump(data.collect(), f, protocol=pickle.HIGHEST_PROTOCOL)

CSS3_NAMES_TO_GROUPS = {
    u'aliceblue': u'blue',
    u'antiquewhite': u'pink',
    u'aqua': u'blue',
    u'aquamarine': u'blue',
    u'azure': u'blue',
    u'beige': u'white',
    u'bisque': u'pink',
    u'black': u'black',
    u'blanchedalmond': u'pink',
    u'blue': u'blue',
    u'blueviolet': u'purple',
    u'brown': u'brown',
    u'burlywood': u'brown',
    u'cadetblue': u'blue',
    u'chartreuse': u'green',
    u'chocolate': u'orange',
    u'coral': u'orange',
    u'cornflowerblue': u'blue',
    u'cornsilk': u'white',
    u'crimson': u'red',
    u'cyan': u'blue',
    u'darkblue': u'blue',
    u'darkcyan': u'blue',
    u'darkgoldenrod': u'brown',
    u'darkgray': u'gray',
    u'darkgrey': u'gray',
    u'darkgreen': u'green',
    u'darkkhaki': u'brown',
    u'darkmagenta': u'purple',
    u'darkolivegreen': u'green',
    u'darkorange': u'orange',
    u'darkorchid': u'purple',
    u'darkred': u'red',
    u'darksalmon': u'pink',
    u'darkseagreen': u'green',
    u'darkslateblue': u'purple',
    u'darkslategray': u'gray',
    u'darkslategrey': u'gray',
    u'darkturquoise': u'blue',
    u'darkviolet': u'purple',
    u'deeppink': u'pink',
    u'deepskyblue': u'blue',
    u'dimgray': u'gray',
    u'dimgrey': u'gray',
    u'dodgerblue': u'blue',
    u'firebrick': u'red',
    u'floralwhite': u'white',
    u'forestgreen': u'green',
    u'fuchsia': u'purple',
    u'gainsboro': u'gray',
    u'ghostwhite': u'white',
    u'gold': u'yellow',
    u'goldenrod': u'yellow',
    u'gray': u'gray',
    u'grey': u'gray',
    u'green': u'green',
    u'greenyellow': u'green',
    u'honeydew': u'blue',
    u'hotpink': u'pink',
    u'indianred': u'red',
    u'indigo': u'purple',
    u'ivory': u'white',
    u'khaki': u'yellow',
    u'lavender': u'gray',
    u'lavenderblush': u'pink',
    u'lawngreen': u'green',
    u'lemonchiffon': u'yellow',
    u'lightblue': u'blue',
    u'lightcoral': u'pink',
    u'lightcyan': u'blue',
    u'lightgoldenrodyellow': u'yellow',
    u'lightgray': u'gray',
    u'lightgrey': u'gray',
    u'lightgreen': u'green',
    u'lightpink': u'pink',
    u'lightsalmon': u'pink',
    u'lightseagreen': u'blue',
    u'lightskyblue': u'blue',
    u'lightslategray': u'gray',
    u'lightslategrey': u'gray',
    u'lightsteelblue': u'blue',
    u'lightyellow': u'yellow',
    u'lime': u'green',
    u'limegreen': u'green',
    u'linen': u'pink',
    u'magenta': u'purple',
    u'maroon': u'red',
    u'mediumaquamarine': u'blue',
    u'mediumblue': u'blue',
    u'mediumorchid': u'purple',
    u'mediumpurple': u'purple',
    u'mediumseagreen': u'green',
    u'mediumslateblue': u'blue',
    u'mediumspringgreen': u'green',
    u'mediumturquoise': u'blue',
    u'mediumvioletred': u'purple',
    u'midnightblue': u'blue',
    u'mintcream': u'white',
    u'mistyrose': u'pink',
    u'moccasin': u'pink',
    u'navajowhite': u'pink',
    u'navy': u'blue',
    u'oldlace': u'pink',
    u'olive': u'green',
    u'olivedrab': u'green',
    u'orange': u'orange',
    u'orangered': u'red',
    u'orchid': u'purple',
    u'palegoldenrod': u'brown',
    u'palegreen': u'green',
    u'paleturquoise': u'blue',
    u'palevioletred': u'pink',
    u'papayawhip': u'pink',
    u'peachpuff': u'pink',
    u'peru': u'brown',
    u'pink': u'pink',
    u'plum': u'purple',
    u'powderblue': u'blue',
    u'purple': u'purple',
    u'red': u'red',
    u'rosybrown': u'purple',
    u'royalblue': u'blue',
    u'saddlebrown': u'brown',
    u'salmon': u'pink',
    u'sandybrown': u'brown',
    u'seagreen': u'green',
    u'seashell': u'pink',
    u'sienna': u'brown',
    u'silver': u'gray',
    u'skyblue': u'blue',
    u'slateblue': u'blue',
    u'slategray': u'gray',
    u'slategrey': u'gray',
    u'snow': u'white',
    u'springgreen': u'green',
    u'steelblue': u'blue',
    u'tan': u'brown',
    u'teal': u'blue',
    u'thistle': u'purple',
    u'tomato': u'red',
    u'turquoise': u'blue',
    u'violet': u'purple',
    u'wheat': u'pink',
    u'white': u'white',
    u'whitesmoke': u'white',
    u'yellow': u'yellow',
    u'yellowgreen': u'green',
}

sc = SparkContext(appName="Men vs Women data")
sqlc = SQLContext(sc)
df_products = sqlc.read.json("/data/doina/UCSD-Amazon-Data/meta_Clothing_Shoes_and_Jewelry.json.gz")
df_images = sqlc.read.json("/user/s1557920/amazonresults_all_json_1516037263.48")

# Combine the two datasets such that we get one with both the product data and the colours
df_products_alias = df_products.alias("df_products")
df_images_alias = df_images.alias("df_images")
joined_df = df_products_alias.join(df_images_alias, col("df_images.asin") == col("df_products.asin"), 'inner')\
    .select("df_products.asin","categories","df_products.price","category","salesrank", "colours" )

def getbycat(filterfunc, name, cat):
    """
    Load the datasets and save their salesrank data per price bucket for a given category
    :param filterfunc: The filter function to apply
    :param name: The name of the resulting file
    :param cat: The category to work with
    :return:
    """
    filterfunc_ = udf(filterfunc, BooleanType())
    filtered = joined_df.filter((joined_df.category == cat) & (joined_df.salesrank.isNotNull())&(joined_df.price.isNotNull()))
    filtered_80 = filtered.filter(filtered.price <80)
    filtered_80_150 = filtered.filter((filtered.price >=80)&(filtered.price<150))
    filtered_150 = filtered.filter(filtered.price >=150)

    only_all = filtered.filter(filterfunc_(filtered.categories))
    only_80 = filtered_80.filter(filterfunc_(filtered_80.categories))
    only_80_150 = filtered_80_150.filter(filterfunc_(filtered_80_150.categories))
    only_150 = filtered_150.filter(filterfunc_(filtered_150.categories))
    owd = only_all.map(lambda x: (CSS3_NAMES_TO_GROUPS[x["colours"][0]["name"]], x["salesrank"])) \
        .groupByKey().mapValues(list) \
        .map(lambda x: (x[0], numpy.median(x[1]), len(x[1])))
    owd_80 = only_80.map(lambda x: (CSS3_NAMES_TO_GROUPS[x["colours"][0]["name"]], x["salesrank"])) \
        .aggregateByKey((0, 0), lambda a, b: (a[0] + b, a[1] + 1), lambda a, b: (a[0] + b[0], a[1] + b[1])) \
        .mapValues(lambda v: (v[0] / v[1], v[1]))
    owd_80_150 = only_80_150.map(lambda x: (CSS3_NAMES_TO_GROUPS[x["colours"][0]["name"]], x["salesrank"])) \
        .groupByKey().mapValues(list) \
        .map(lambda x: (x[0], numpy.median(x[1]), len(x[1])))
    owd_150 = only_150.map(lambda x: (CSS3_NAMES_TO_GROUPS[x["colours"][0]["name"]], x["salesrank"])) \
        .groupByKey().mapValues(list) \
        .map(lambda x: (x[0], numpy.median(x[1]), len(x[1])))

    savetopickle(owd, name+"_all")
    savetopickle(owd_80, name+"_80")
    savetopickle(owd_80_150, name+"_80_150")
    savetopickle(owd_150, name+"_150")

getbycat(findWomen, "only_women", "Shoes")
getbycat(findMen, "only_men", "Shoes")
