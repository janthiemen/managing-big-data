import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import cPickle as pickle
import math

FOLDER = "plots_avg_median_histogram"

N=10000
with open('pickles/most_dominant_colours_avg_median.pickle', 'rb') as handle:
    results = pickle.load(handle)
    results_mapped = {}
    for result in results:
        if result[0] not in results_mapped.keys():
            results_mapped[result[0]] = []
        results_mapped[result[0]].append(result[1])


    for catcolour in results_mapped.keys():
        ranks = results_mapped[catcolour]
        counts = {}
        for rank in ranks:
            rank_bucket = int(math.floor(rank/N))
            if rank_bucket not in counts:
                counts[rank_bucket] = 0
            counts[rank_bucket] += 1

        axis = counts.keys()
        x = counts.values()
        # the histogram of the data
        plt.bar(axis, x, width=10.0)
        plt.xlabel('Bucket (' + str(N) + ' sales ranks each)')
        plt.ylabel('Count of products/bucket')
        plt.title('Sales ranks for '+catcolour)

        plt.grid(True)

        plt.savefig(FOLDER + "/" + catcolour + '.png')
        plt.clf()

"""
>>> most_dominant_colours = df.select("colours", "category", "salesrank")\
...     .map(lambda x: (x["category"]+" "+CSS3_NAMES_TO_GROUPS[x["colours"][0]["name"]], x["salesrank"]))
>>> most_dominant_colours.take(1)
"""