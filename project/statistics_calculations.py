from pyspark import SparkContext
from pyspark import SQLContext
import numpy
import pickle
import plots_calculations.CSS3_NAMES_TO_GROUPS as customColorMapper

path = "file:///home/s1557920/allresults"
sc = SparkContext(appName="Amazon colours")
sqlc = SQLContext(sc)
df = sqlc.read.json(path)

total_product_count = df.select("asin").count() #3.165.473

categories_amount = df.select("category").distinct().count() #31

count_per_category = df.select("category")\
	.map(lambda x: (x[0],1))\
    .reduceByKey(lambda x,y: x + y)

"""
Clothing	                742001
Sports &amp; Outdoors	    440770
Shoes	                    373762
Cell Phones & Accessories	339165
Toys & Games	            335517
Home &amp; Kitchen	        254055
Jewelry	                    244415
Kitchen & Dining	        182676
Watches	                    79396
Electronics	                65047
Industrial & Scientific	    21904
Camera &amp; Photo	        20648
Health & Personal Care	    15039
Patio Lawn & Garden	        12182
Arts Crafts & Sewing	    8374
Computers & Accessories	    6322
Home Improvement	        5134
Beauty	                    4849
Musical Instruments	        3354
Pet Supplies	            2913
Video Games	                2180
Music	                    1556
Automotive	                1440
Software	                1058
Office Products	            704
Baby	                    691
Movies & TV	                202
Appliances	                72
Grocery & Gourmet Food	    25
Books	                    20
Magazines	                2
"""

most_dominant_color_distribution = df.select("colours")\
    .map(lambda x: (x[0][0]["name"], 1))\
    .reduceByKey(lambda x,y: x + y) \
    .sortBy(lambda x: x[1], ascending=False)

most_dominant_color_distribution_mapped = df.select("colours")\
    .map(lambda x: (customColorMapper[x[0][0]["name"]], 1)) \
    .reduceByKey(lambda x, y: x + y) \
    .sortBy(lambda x: x[1], ascending=False)

most_dominant_colors_distribution = df.select("colours").sample(False, 0.001)\
    .map(lambda x: (x[0][0]["name"], 1)) \
    .reduceByKey(lambda x, y: x + y) \
    .sortBy(lambda x: x[1], ascending=False)


most_dominant_colors_distribution_mapped = df.select("colours")


average_price_per_color = df.select("colours", "price")